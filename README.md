# A ChristWoo Web Clone Chat Application for those developers that like to use Django Channel for handling WebSocket request



## Installation 
```
cd christwoo-backend/

source venv/bin/activate

python3 -m pip install requirements.txt

or pip3 install -r requirements.txt

```

## How to run development server?

#### create all the required tables
```
python3 manage.py migrate
```

#### start redis service
```
docker run -p 6379:6379 -d redis:5
```

#### run the development server
```
python3 manage.py runserver
```



